import { exer1 } from '../exos/exo_1.js';
import { exer2 } from '../exos/exo_2.js';
import { exer3 } from '../exos/exo_3.js';
import { exer4 } from '../exos/exo_4.js';
import { exer5 } from '../exos/exo_5.js';
import { exer6 } from '../exos/exo_6.js';
import { exer7 } from '../exos/exo_7.js';
import { exer8 } from '../exos/exo_8.js';
import { funk1 } from '../exos/functionexo_1.js';
import { funk2 } from '../exos/functionexo_2.js';
import { funk3 } from '../exos/functionexo_3.js';
import { funk4 } from '../exos/functionexo_4.js';
import { funk5 } from '../exos/functionexo_5.js';
import { funk6 } from '../exos/functionexo_6.js';
import { funk7 } from '../exos/functionexo_7.js';
import { funk8 } from '../exos/functionexo_8.js';

import { annee } from '../exos/tableauexo_1.js';
import { tablo2 } from '../exos/tableauexo_2.js';
import { tablo3 } from '../exos/tableauexo_3.js';
import { tablo4 } from '../exos/tableauexo_4.js';
import { associatif } from '../exos/tableauexo_5.js';
import { tablo6 } from '../exos/tableauexo_6.js';
import { tablo7 } from '../exos/tableauexo_7.js';
import { tablo8 } from '../exos/tableauexo_8.js';
import { tablo9 } from '../exos/tableauexo_9.js';
// import { tablo10 } from '../exos/tableauexo_10.js';
// import { tablo8 } from '../exos/tableauexo_8.js';

exer1();
exer2();
exer3();
exer4();
exer5();
exer6();
exer7();
exer8();

funk1(console.log(funk1()));
funk2();
funk3(console.log(funk3('kapapouette', 'pouette')));
funk4(console.log(funk4(6, 6)));
funk5(console.log(funk5(10, 'pouette')));
funk6(console.log(funk6("nommm", "prenom", 0)));
funk7(console.log(funk7(19, 'homme')));
funk8();

tablo2(annee);
tablo3(annee);
tablo4(annee);

tablo6(associatif);
tablo7(associatif);
tablo8(annee);
tablo9(associatif);
// tablo10(associatif);